indicator:
  name: Mon quartier à pied
  description: |
    Cette couche traduit la "perméabilité" du réseau ouvert aux piétons, 
    c'est-à-dire la quantité de détours en moyenne à faire pour se déplacer par rapport au vol d'oiseau, 
    en coloriant les ilots formés par le réseau, en blanc pour les zones non habitées (densité < 100&nbsp;hab/km<sup>2</sup>), 
    et en allant du rouge pour les grands ilots au vert pour les petits ilots. 

    La taille de l'ilot est une condition nécessaire (mais pas suffisante) pour la marche à pied : 
    même dans un quartier agréable, il y aura peu de piétons si le périmètre des ilots dépasse 1&nbsp;km.

    Les ilots sont calculés à partir des données de voirie OpenStreetMap, il peut y avoir des erreurs dues à l'algorithme ou des corrections à apporter aux données OSM.

    | condition | couleur |
    | ------------ | ------------ |
    | densité de pop < 100 hab/km2 | <div style="--color: white" class="legend-square" /> |
    | périmètre de l'ilot < 350 m | <div style="--color: #078C00" class="legend-square" /> |
    | périmètre entre 350 et 700 m | <div style="--color: #E3E73B" class="legend-square" /> |
    | ------------ | ------------ |
    | périmètre > 700 m et densité < 1000 hab/km2 | <div style="--color: #FFA295" class="legend-square" /> |
    | périmètre > 700 m et densité entre 1000 et 10000 | <div style="--color: #E7503B" class="legend-square" /> |
    | périmètre > 700 m et densité > 10000 | <div style="--color: red" class="legend-square" /> |

    <u>Sources : </u>
    - voirie www.openstreetmap.org via geofabrik.de
    - densité de population par carreaux de 200mx200m Insee https://www.data.gouv.fr/fr/datasets/donnees-carroyees-a-200m-sur-la-population/
    ilots et score calculés par les indicateurs OMI


  popupTemplate: |
    - <div style="display: flex; margin: 0;">
        Score :
        <div style="--color: <%= it.color %>; margin: 0 0.5em;" class="legend-square"></div>
        <%= it.score %>
      </div>
    - Périmètre : <b><%= it.perimetre %> mètres</b>
    - Densité de population : <%= it.densite %> hab/km<sup>2</sup>
