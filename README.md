# "Mon quartier a pied" score Open Mobility indicator

## description : [indicator.yml FILE](https://gitlab.com/open-mobility-indicators/indicators/mon-quartier-a-pied/-/blob/main/indicator.yaml)

This python notebook just adds a walkability score and color (derived from pop density and cycle length)
to a pre-computed (pedestrian) network blocks/cycles geojsonfile (produced by the population-density-from-cycles notebook).
(the OMI framework then produces vector tiles as .mbtiles files from the geoJSON file).

forked from [jupyter notebook template](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook)

[Install locally using a venv or Docker, Build and Use (download, compute)](https://gitlab.com/open-mobility-indicators/website/-/wikis/2_contributeur_technique/install-a-notebook-locally)
